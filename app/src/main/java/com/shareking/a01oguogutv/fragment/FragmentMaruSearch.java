package com.shareking.a01oguogutv.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fsn.cauly.CaulyAdInfo;
import com.fsn.cauly.CaulyAdInfoBuilder;
import com.fsn.cauly.CaulyAdView;
import com.fsn.cauly.CaulyCloseAd;
import com.fsn.cauly.CaulyInterstitialAd;
import com.fsn.cauly.CaulyInterstitialAdListener;
import com.shareking.a01oguogutv.R;
import com.shareking.a01oguogutv.activity.list.QooListActivity;
import com.shareking.a01oguogutv.adapter.ListDtamaAdapter;
import com.shareking.a01oguogutv.item.ListDramaItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentMaruSearch extends Fragment implements View.OnClickListener, CaulyInterstitialAdListener {
    private final String TAG = " FragmentMaruSearch - ";
    private ProgressDialog mProgressDialog;
    private ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";
    private String searchUrl = "";
    private String keyword1 = "";
    private ArrayList<ListDramaItem> listViewItemArr;

    private EditText editText;
    private Button searchBtn;

    InputMethodManager inputManager;

    private int adsCnt = 0;
    SharedPreferences pref;

    ArrayAdapter<String> adapter;

    private int adn = 0;

    private String intentListUrl = "";
    private String intentImgUrl = "";
    private String intentTitle = "";

    // spinner
    private Spinner spinner2;
    ArrayList<String> arraySpinnerList;
    ArrayAdapter<String> arraySpinnerAdapter;
    int selectedSpinner = 0;

    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=1500;
    private long mLastClickTime;

    // cauly
    private static final String APP_CODE = "r64fmbSI"; // Jwi7uDAD
    private CaulyAdView adView;
    private CaulyCloseAd mCloseAd ;
    private boolean showInterstitial = false;
    private CaulyInterstitialAd interstial;
    private CaulyAdInfo adInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maru_search, container, false);

        baseUrl = getArguments().getString("baseUrl");
        Log.d(TAG, "baseUrl : " + baseUrl);
        adsCnt = getArguments().getInt("adsCnt");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));


        listView = (ListView)view.findViewById(R.id.listview);

        editText = (EditText)view.findViewById(R.id.fr20_edit);
        searchBtn = (Button)view.findViewById(R.id.fr20_searchbtn);
        searchBtn.setOnClickListener(this);

        inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        arraySpinnerList = new ArrayList<>();
        arraySpinnerList.add("드라마 검색");
        arraySpinnerList.add("예능 검색");
        arraySpinnerList.add("시사 검색");
        arraySpinnerList.add("영화 검색");
        arraySpinnerList.add("미드 검색");

        searchUrl = baseUrl + "&bo_table=drama_new&stx=";

        arraySpinnerAdapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_dropdown_item, arraySpinnerList);

        spinner2 = (Spinner)view.findViewById(R.id.spinner2);
        spinner2.setAdapter(arraySpinnerAdapter);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "selected spinner : " + i);
                selectedSpinner = i;
                if(selectedSpinner == 0){ searchUrl = baseUrl + "&bo_table=drama_new&stx="; }
                else if(selectedSpinner == 1) { searchUrl = baseUrl + "&bo_table=enter_new&stx="; }
                else if(selectedSpinner == 2) { searchUrl = baseUrl + "&bo_table=news_new&stx="; }
                else if(selectedSpinner == 3) { searchUrl = baseUrl + "&bo_table=movie&stx="; }
                else if(selectedSpinner == 4) { searchUrl = baseUrl + "&bo_table=mid&stx="; }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        //// paging ////
        tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        return view;
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<ListDramaItem>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                Log.d(TAG, "searched Url  : " + searchUrl + keyword1 + "&page=" + pageNum);
                doc = Jsoup.connect(searchUrl + keyword1 + "&page=" + pageNum).timeout(10000).get();

                if(selectedSpinner == 3 || selectedSpinner == 4){   // movie or mid
                    Elements elements = doc.select(".bs4_gallery .img-thumbnail");

                    for(Element element: elements) {
                        String title = element.select(".caption").text();
                        String imgUrl = AddHttps(element.select("img").attr("src"));
                        String listUrl = element.select("a").attr("href").split("&page=")[0];

                        //Log.d(TAG, "title : " + title);
                        ListDramaItem listViewItemList = new ListDramaItem(title, imgUrl, listUrl);
                        listViewItemArr.add(listViewItemList);
                    }
                } else {
                    Elements elements = doc.select(".board_list ul li");

                    for(Element element: elements) {
                        String title = element.select(".bo_subject a").text();
                        String imgUrl = AddHttps(element.select(".bo_pf_img img").attr("src"));
                        String listUrl = element.select(".bo_subject a").attr("href").split("&page=")[0];

                        if(title == null || title.equals("")) continue;

                        Log.d(TAG, "title : " + title);
                        ListDramaItem listViewItemList = new ListDramaItem(title, imgUrl, listUrl);
                        listViewItemArr.add(listViewItemList);
                    }
                }

                ////////////// get lat page /////////////
                if(lastPage.equals("1")){

                    if(selectedSpinner == 3 || selectedSpinner == 4) {   // movie or mid
                        Elements elements = doc.select(".pagination .page-link");

                        for(Element element: elements) {
                            String title = element.text();
                            if(!title.equals("맨끝")) continue;
                            lastPage = element.attr("href").split("&page=")[1];
                        }
                    } else {
                        Elements elements = doc.select(".pg_wrap .pg_end");
                        Log.d(TAG, "elements.size : " + elements.size());
                        if(elements.size() == 0){
                            lastPage = "1";
                        } else {
                            lastPage = elements.attr("href").split("&page=")[1];
                        }
                    }

                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if(getActivity() != null){

                //// paging ////
                tv_currentPage.setText(pageNum+"");
                tv_lastPage.setText(lastPage);

                if(listViewItemArr.size() == 0){
                    keyword1 = "";
                    listViewItemArr.clear();
                    ListDtamaAdapter adapter = new ListDtamaAdapter(getActivity(), listViewItemArr, R.layout.item_list_drama);
                    listView.setAdapter(adapter);
                } else {
                    listView.setAdapter(new ListDtamaAdapter(getActivity(), listViewItemArr, R.layout.item_list_drama));

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            long currentClickTime= SystemClock.uptimeMillis();
                            long elapsedTime=currentClickTime-mLastClickTime;
                            mLastClickTime=currentClickTime;
                            // 중복 클릭인 경우
                            if(elapsedTime<=MIN_CLICK_INTERVAL){
                                return;
                            }

                            adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                            if(adsCnt == 1){
                                adsCnt++;
                                SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                                editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                                editor.commit(); //완료한다.

                                intentListUrl = listViewItemArr.get(position).getListUrl();
                                intentImgUrl = listViewItemArr.get(position).getImgUrl();
                                intentTitle = listViewItemArr.get(position).getTitle();

                                //interstitialAd.show();
                                adFull();

                            } else {
                                intentListUrl = listViewItemArr.get(position).getListUrl();
                                intentImgUrl = listViewItemArr.get(position).getImgUrl();
                                intentTitle = listViewItemArr.get(position).getTitle();

                                Intent intent = new Intent(getActivity(), QooListActivity.class);
                                intent.putExtra("listUrl", intentListUrl);
                                intent.putExtra("title", intentTitle);
                                intent.putExtra("imgUrl", intentImgUrl);
                                intent.putExtra("adsCnt", "2");
                                startActivity(intent);
                            }
                        }
                    });
                }
            }

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;
            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;
            case R.id.fr20_searchbtn :
                if(!editText.getText().toString().equals("")){
                    keyword1 = editText.getText().toString().trim();
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                } else {
                    Toast.makeText(getActivity(), "검색어를 입력하세요.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    public void adFull(){
        adInfo = new CaulyAdInfoBuilder(APP_CODE).build();
        interstial = new CaulyInterstitialAd();
        interstial.setAdInfo(adInfo);
        interstial.setInterstialAdListener(this);
        interstial.disableBackKey();
        interstial.requestInterstitialAd(getActivity());
        showInterstitial = true;
    }

    @Override
    public void onReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, boolean b) {
        if (b == false) {
            Log.d("dddd", "free interstitial AD received.free interstitial AD received.");
        } else {
            Log.d("dddd", "normal interstitial AD received.");
        }

        if (showInterstitial){
            caulyInterstitialAd.show();
        } else {
            caulyInterstitialAd.cancel();
        }
    }

    @Override
    public void onFailedToReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, int i, String s) {
        showInterstitial = false;
        Log.d("ffff", "free interstitial AD received.free interstitial AD received.");
        adn += 10;
        if(adn < 100){
            adFull();
        } else {
            showInterstitial = false;
            adn = 0;
        }
    }

    @Override
    public void onClosedInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {

    }

    @Override
    public void onLeaveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {

    }

    public static String AddHttps(String baseUrl){
        String resultUrl = baseUrl;
        if(!baseUrl.contains("https")) resultUrl = "https:" + resultUrl;

        return resultUrl;
    }
}
